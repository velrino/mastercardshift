<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::group(array('before' => 'token'), function()
{
	Route::get('/', 'AppController@index');

	Route::group(array('prefix' => 'auth'), function()
	{
		Route::post('/register', 'AuthController@register');
		Route::get('/register', 'AuthController@registerConfirma');
		Route::post('/login', 'AuthController@login');
		Route::post('/facebook', 'AuthController@registerFacebook');

		Route::group(array('prefix' => 'password'), function()
		{
			Route::post('/forget', 'AuthController@forgetPassword');
			Route::put('/forget', 'AuthController@forgetPasswordChange');
		});

	});

});



Route::group(array('prefix' => 'auth2'), function()
{
	Route::post('/logar', 'AuthController@criarSessaso');

});


//Route::get('/generate/models', '\\Jimbolino\\Laravel\\ModelBuilder\\ModelGenerator4@start');
