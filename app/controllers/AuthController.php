<?php

class AuthController extends BaseController {


	public function criarSessaso()
	{

		if(!Session::has('usuario')) Session::push('usuario', Input::all());
		if(Session::has('usuario')) Session::put('usuario', Input::all());

		return Response::json(Session::get('usuario'));
	}

	/**
  * @apiDefine Autenticacao Autenticação
 */
  function UsuarioToken( $idUsuario, $idOneSignal = null, $parameters = null )
  {
		//  if( is_null($parameters) ) $parameters = Login::leftJoin('usuarios', 'login.idUsuario', '=', 'usuarios.idUsuario')->where('usuarios.idUsuario',"=",$idUsuario)->first()->toArray();
  	$parameters = Usuarios::where('idUsuario',"=",$idUsuario)->first()->toArray();
    if(isset($parameters['senha'])) unset($parameters['senha']);

    $token = TokenController::getToken($parameters['idUsuario'], $idOneSignal);
    // $parameters['notificacao'] = Notificacoes::select('idNotificacoes','tipo', 'visualizado')->where('idReceptor', $parameters['idUsuario'])->get()->toArray();
    // if( empty( $parameters['notificacao'] ) ) $parameters['notificacao'] = "";
    $result['usuario'] =  $parameters;
    // $result = AppController::array_swap_assoc( 'idUsuario', key($result), $result );
    $result['token'] = $token["token"];
    $result['message'] = 'Login efetuado com sucesso';
    return AppController::retorno( $result );
  }
  /**
   * @apiDefine Autenticacao Autenticação
  */
   /**
    * @api {post} /auth/register 1 - Cadastro
    * @apiVersion 1.0.0
    * @apiDescription Para o usuário efetuar o cadastro.
    * @apiGroup Autenticacao
    * @apiName 1 - Cadastro
    * @apiHeader {String} token Token fornecido pela FolkSpace.
    *
    * @apiParamExample {json} Tipo
    * {
    *     "email" : "STRING",
    *     "senha" : "STRING"
    * }
    * @apiParamExample {json} Exemplo
    * {
    *     "email" : "nome@email.com",
    *     "senha" : "skyrim"
    * }
    * @apiError (Header) {Integer} 400 Bad Request.
    * @apiError (Parâmetros) {Integer} message Mensagem do Erro.
    * @apiError (Parâmetros) {Integer} code Codigo do erro.
    * @apiErrorExample {json} Erro
    * {
    *     "errors": [
    *         {
    *             "message": "E-mail informado já está sendo utilizado por outro usuário",
    *             "code": 4
    *         }
    *     ]
    * }
    * @apiError (Parâmetros) {Integer} idUsuario Id do Usuário.
    * @apiSuccess (Header) {Integer} 200 Ok.
    * @apiSuccessExample {json} Tipo
    *  {
    *      "message": "STRING"
    *  }
    * @apiSuccessExample {json} Exemplo
    *  {
    *      "message": "Verifique sua caixa de entrada"
    *  }
    */
	public function register()
	{
		$request = AppController::verificaRequest( Input::all() );
    if($request['status'] != 200)   return AppController::retorno($request['message'], 400 );
    $request = $request["result"];
		/* Body obrigatórios */
		$response['email'] 	= (empty($request['email'])) 	? "Por favor, informe o Email ? 1" : NULL ;
		$response['senha'] 	= (empty($request['senha'])) 	? "Por favor, informe a Senha ? 1" : NULL ;
    //$response['idOneSignal'] 	= (empty($request['idOneSignal'])) 	? "Por favor, informe a idOneSignal ? 1" : NULL ;

		$response = array_values( array_filter($response));
    if(!empty($response)) return AppController::retorno( $response , 400);
		/* Verifica se o E-mail informado é valido */
		$response['failMail']  = (!filter_var($request['email'], FILTER_VALIDATE_EMAIL)) ? "Por favor, informe um e-mail válido! ? 2" : NULL ;
		/* Verifica se o E-mail informado já existe em outro usuário */
    $response['usuario'] 	= (!is_null(Usuarios::where('email', $request['email'])->first())) 	? "E-mail informado já está sendo utilizado por outro usuário ? 3" : NULL ;
		$response = array_values( array_filter($response));
    /* Retorna apenas se existir algum erro */
    if(!empty($response)) return AppController::retorno( $response , 400);
    /* Funcionalidade para enviar o e-mail */
    // Criando usuário
		$createUsuario = (array) Usuarios::add( (array) $request);

    $codigo = AppController::random($l = 6);
    $solicitacao =
    [
      'tipo' => 'cadastro',
      'codigo' => $codigo,
      'idUsuario' => $createUsuario['idUsuario']
    ];
    //Cria solicitação
    //Solicitacoes::add($solicitacao);
    //Encaminha e-mail
    Mail::send('emails.welcome',
    array(
      'codigo' => $codigo,
      'email' => Input::get('email')
    ),
    function($message)
    {
      $message->to(
      Input::get('email') //Destinatario
      ,
      Input::get('email') //Para
      )->subject('Bem-vindo ao NuStore');
    });
    //$result['message'] = 'Verifique sua caixa de entrada';
    return AuthController::UsuarioToken( $createUsuario['idUsuario'] );
    //return AppController::retorno( $result );
	}
  /**
   * @apiDefine Autenticacao Autenticação
  */
   /**
    * @api {get} /auth/register?codigo=:codigo 2 - Cadastro confirmar código
    * @apiVersion 1.0.0
    * @apiDescription Para o confirmar o cadastro do usuário.
    * @apiGroup Autenticacao
    * @apiName 2 - Cadastro confirmar código
    * @apiHeader {String} token Token fornecido pela FolkSpace.
    * @apiParam {String} codigo Código recebido na caixa de entrada do e-mail.

    * @apiError (Header) {Integer} 400 Bad Request.
    * @apiError (Parâmetros) {Integer} message Mensagem do Erro.
    * @apiError (Parâmetros) {Integer} code Codigo do erro.
    * @apiErrorExample {json} Erro
    * {
    *     "errors": [
    *         {
    *             "message": "Código inválido",
    *             "code": 4
    *         }
    *     ]
    * }
    * @apiError (Parâmetros) {Integer} idUsuario Id do Usuário.
    * @apiSuccess (Header) {Integer} 200 Ok.
    * @apiSuccessExample {json} Tipo
    *  {
    *      "message": "STRING"
    *  }
    * @apiSuccessExample {json} Exemplo
    *  {
    *      "message": "Parabéns, você está ativo!"
    *  }
    */
  function registerConfirma()
  {
      $response['codigo'] 	= (empty($_GET['codigo'])) 	? "Por favor, informe o código ? 1" : NULL ;

      $response = array_values( array_filter($response));
      if(!empty($response)) return AppController::retorno( $response , 400);

     $Solicitaco = Solicitacoes::where('codigo', $_GET['codigo'])->where('tipo', 'cadastro')->first();

     $response['Solicitacoes'] 	= (is_null($Solicitaco)) 	? "Código inválido ? 4" : NULL ;
     $response = array_values( array_filter($response));
     if(!empty($response)) return AppController::retorno( $response , 400);

     $Solicitaco = $Solicitaco->toArray();
     if($Solicitaco['ativo'] != 0) $response['codigo'] =  "Código já utilizado ? 4";
     $response = array_values( array_filter($response));
     if(!empty($response)) return AppController::retorno( $response , 400);

     $login = Login::where('idUsuario', $Solicitaco['idUsuario'])->where('ativo', 0)->update(['ativo' => 1]);
     $usuario = Login::where('idUsuario', $Solicitaco['idUsuario'])->where('ativo', 1)->first()->toArray();
     Solicitacoes::where('codigo', $_GET['codigo'])->where('tipo', 'cadastro')->delete();
     $result['message'] = 'Parabéns, você está ativo!';
     return AppController::retorno( $result );
   }
  /**
   * @apiDefine Autenticacao Autenticação
  */
   /**
    * @api {post} /auth/login 3 - Login
    * @apiVersion 1.0.0
    * @apiDescription Para o usuário cadastrado efetuar o login.
    * @apiGroup Autenticacao
    * @apiName 3 - Login
    * @apiHeader {String} token Token fornecido pela FolkSpace.
    *
    * @apiParamExample {json} Tipo
    * {
    *     "idOneSignal":"STRING"
    *     "email" : "STRING",
    *     "senha" : "STRING",
    * }
    * @apiParamExample {json} Exemplo
    * {
    *     "idOneSignal":"92198129819"
    *     "email" : "nome@email.com",
    *     "senha" : "skyrim",
    * }
    * @apiError (Header) {Integer} 400 Bad Request.
    * @apiError (Parâmetros) {Integer} message Mensagem do Erro.
    * @apiError (Parâmetros) {Integer} code Codigo do erro.
    * @apiErrorExample {json} Erro
    * {
    *     "errors": [
    *         {
    *             "message": "Senha informada está incorreta",
    *             "code": 4
    *         }
    *     ]
    * }
    * @apiError (Parâmetros) {Integer} idUsuario Id do Usuário.
    * @apiSuccess (Header) {Integer} 200 Ok.
    * @apiSuccessExample {json} Tipo
    *  {
    *      "usuario": {
    *          "idLogin": INT,
    *          "email": "STRING",
    *          "flagFacebook": INT,
    *          "emailFacebook": "STRING",
    *          "idFacebook": "STRING",
    *          "tokenFacebook": "STRING",
    *          "idUsuario": INT,
    *          "idApp": INT,
    *          "nome": "STRING",
    *          "foto": "BASE 64",
    *          "dataCadastro": "DATETIME",
    *          "dataAlteracao": "DATETIME",
    *          "admin": INT,
    *          "cpf": "STRING",
    *          "dataNascimento": "STRING"
    *      },
    *      "token": "STRING",
    *      "message": "STRING"
    *  }
    * @apiSuccessExample {json} Exemplo
    *  {
    *      "usuario": {
    *          "idLogin": 25,
    *          "email": "email@gmail.com",
    *          "flagFacebook": 0,
    *          "emailFacebook": "",
    *          "idFacebook": "",
    *          "tokenFacebook": "",
    *          "idUsuario": 26,
    *          "idApp": 1,
    *          "nome": "Billy Batson",
    *          "foto": "",
    *          "dataCadastro": "2016-04-25 04:58:11",
    *          "dataAlteracao": "2016-04-25 04:58:11",
    *          "admin": 0,
    *          "cpf": 12345678,
    *          "dataNascimento": ""
    *      },
    *      "token": "STRING",
    *      "message": "Login efetuado com sucesso"
    *  }
    */
	public function login()
	{
    $request = AppController::verificaRequest( Input::all() );
    if($request['status'] != 200)   return AppController::retorno($request['message'], 400 );
    $request = $request["result"];

    $response['email'] 	= (empty($request['email'])) 	? "Por favor, informe o E-mail ? 1" : NULL ;
    $response['senha'] 	= (empty($request['senha'])) 	? "Por favor, informe a senha ? 2" : NULL ;

    $response = array_values( array_filter( $response ) );
	  if(!empty($response)) return AppController::retorno( $response , 400);
		/* Verifica se o E-mail informado é valido */
		$response['failLogin']  = (!filter_var($request['email'], FILTER_VALIDATE_EMAIL)) ? "Por favor, informe um e-mail válido! ? 2" : NULL ;
    $response = array_values( array_filter( $response ) );
	  if(!empty($response)) return AppController::retorno( $response , 400);
		/* Verifica se o E-mail informado já existe em outro usuário */
    $verifyLogin =  Login::where('email', $request['email'])->first();

    //if($verifyLogin['ativo'] != 1 AND !is_null($verifyLogin) ) 	$response['senha'] = "Seu usário não foi verificado com o código ? 99" ;

		if(is_null($verifyLogin))	$response['usuario'] 	=  "E-mail informado não existe ? 4";
			if(empty($request['senha']))
					$response['senha'] = "Por favor, informe a Senha ? 1" ;
			if(isset($verifyLogin['senha']) AND !empty($request['senha']))
				if($verifyLogin['senha'] != md5($request['senha']))	$response['senha'] 	=  "Senha informada está incorreta ? 2" ;
				if(isset($request['senha']) AND $verifyLogin['flagFacebook'] == 1)	$response['senha'] 	=  "Senha informada está incorreta ? 2" ;

    $response = array_values( array_filter( $response ) );
    /* Retorna apenas se existir algum erro */
	  if(!empty($response)) return AppController::retorno( $response , 400);
    if($verifyLogin->ativo != 1)  return AppController::retorno( ["Usuário não ativado? 5 "] , 400);
    $verifyLogin = $verifyLogin->toArray();

		return AuthController::UsuarioToken( (array) $verifyLogin['idUsuario'], null );
	}
  /**
   * @apiDefine Autenticacao Autenticação
  */
   /**
    * @api {post} /auth/facebook 4 - Facebook Login/Cadastro
    * @apiVersion 1.0.0
    * @apiDescription Para o usuário cadastrado ou efetuar o login via Facebook.
    * @apiGroup Autenticacao
    * @apiName 4 - Facebook Login/Cadastro
    * @apiHeader {String} token Token fornecido pela FolkSpace.
    *
    * @apiParamExample {json} Tipo
    * {
    *     "facebook_email" : "STRING",
    *     "facebook_id" : "STRING"
    * }
    * @apiError (Header) {Integer} 400 Bad Request.
    * @apiError (Parâmetros) {Integer} message Mensagem do Erro.
    * @apiError (Parâmetros) {Integer} code Codigo do erro.
    * @apiErrorExample {json} Erro
    * {
    *     "errors": [
    *         {
    *             "message": "E-mail informado já está sendo utilizado por outro usuário",
    *             "code": 4
    *         }
    *     ]
    * }
    * @apiError (Parâmetros) {Integer} idUsuario Id do Usuário.
    * @apiSuccess (Header) {Integer} 200 Ok.
    * @apiSuccessExample {json} Tipo
    *  {
    *      "usuario": {
    *          "idLogin": INT,
    *          "email": "STRING",
    *          "flagFacebook": INT,
    *          "emailFacebook": "STRING",
    *          "idFacebook": "STRING",
    *          "tokenFacebook": "STRING",
    *          "idUsuario": INT,
    *          "idApp": INT,
    *          "nome": "STRING",
    *          "foto": "BASE 64",
    *          "dataCadastro": "DATETIME",
    *          "dataAlteracao": "DATETIME",
    *          "admin": INT,
    *          "cpf": "STRING",
    *          "dataNascimento": "STRING"
    *      },
    *      "token": "STRING",
    *      "message": "STRING"
    *  }
    * @apiSuccessExample {json} Exemplo
    *  {
    *      "usuario": {
    *          "idLogin": 25,
    *          "email": "email@gmail.com",
    *          "flagFacebook": 0,
    *          "emailFacebook": "",
    *          "idFacebook": "",
    *          "tokenFacebook": "",
    *          "idUsuario": 26,
    *          "idApp": 1,
    *          "nome": "Billy Batson",
    *          "foto": "",
    *          "dataCadastro": "2016-04-25 04:58:11",
    *          "dataAlteracao": "2016-04-25 04:58:11",
    *          "admin": 0,
    *          "cpf": 12345678,
    *          "dataNascimento": ""
    *      },
    *      "token": "STRING",
    *      "message": "Login efetuado com sucesso"
    *  }
    */
	public function registerFacebook()
	{
    $request = AppController::verificaRequest( Input::all() );
    if($request['status'] != 200)   return AppController::retorno($request['message'], 400 );
    $request = $request["result"];
    /* Body obrigatórios */
    $response['facebook_id'] 	= (empty($request['facebook_id'])) 	? "Por favor, informe a facebook_id ? 1" : NULL ;
    $response['facebook_email'] 	= (empty($request['facebook_email'])) 	? "Por favor, informe a facebook_email ? 1" : NULL ;
    $response = array_values( array_filter($response));
    if(!empty($response)) return AppController::retorno( $response , 400);

    $response['usuario'] 	= (!is_null(Usuarios::where('email', $request['facebook_email'])->first())) 	? "E-mail informado já está sendo utilizado por outro usuário ? 3" : NULL ;
		$response = array_values( array_filter($response));
    /* Retorna apenas se existir algum erro */
    if(!empty($response)) return AppController::retorno( $response , 400);

    /* Verifica se o Id do facebook informado já existe em outro usuário */
    $query = Usuarios::where('facebook_id', $request['facebook_id'])->first();
    if(!is_null($query)) return AuthController::loginFacebook( $request, $query->toArray() );
    // Criando usuário
    $createUsuario = (array) Usuarios::add( (array) $request);
    $request['idUsuario'] = $createUsuario['idUsuario'];

    if(empty($request['idOneSignal'])) $request['idOneSignal'] = date("Y-m-d h:i:s");
    $request['facebook'] = 1;
		return AuthController::UsuarioToken( (array) $createUsuario['idUsuario'], $request["idOneSignal"] );
	}

	public function loginFacebook($input, $query)
	{
    unset($input['facebook_id']);
    $usuario = Usuarios::where('idUsuario', $query['idUsuario'])->first();
    $usuario->fill($input);
    $usuario->save();

    if(empty($input['idOneSignal'])) $input['idOneSignal'] = date("Y-m-d h:i:s");
		return AuthController::UsuarioToken( $query['idUsuario'], $input["idOneSignal"] );
	}
  /**
 * @api {post} /auth/password/forget 1 - Solicita
 * @apiVersion 1.0.0
 * @apiDescription Para o usuário cadastrado solicitar a alteração da senha.
 * @apiGroup Esqueci a senha
 * @apiName 1 - Solicita
 * @apiHeader {String} token Token fornecido pela ParApps.
 *
 * @apiParamExample {json} Tipo
 * {
 *     "email" : "STRING"
 * }
 * @apiParamExample {json} Exemplo
 * {
 *     "email" : "mail@email.com"
 * }
 * @apiError (Header) {Integer} 400 Bad Request.
 * @apiError (Parâmetros) {Integer} message Mensagem do Erro.
 * @apiError (Parâmetros) {Integer} code Codigo do erro.
 * @apiErrorExample {json} Erro
 * {
 *     "errors": [
 *         {
 *             "message": "E-mail informado não existe ",
 *             "code": 3
 *         }
 *     ]
 * }
 * @apiError (Parâmetros) {Integer} idUsuario Id do Usuário.
 * @apiSuccess (Header) {Integer} 200 Ok.
 * @apiSuccessExample {json} Tipo
 *  {
 *      "message": "STRING"
 *  }
 * @apiSuccessExample {json} Exemplo
 *  {
 *       "message": "Verifique sua caixa de entrada"
 *  }
 */
function forgetPassword()
{
  $request = AppController::verificaRequest( Input::all() );
  if($request['status'] != 200)   return AppController::retorno($request['message'], 400 );
  $request = $request["result"];

  $response['email'] 	= (empty($request['email'])) 	? "Por favor, informe o e-mail ? 1" : NULL ;

  if(!empty($request['email']))
  {
    $response['failLogin'] = (!filter_var($request['email'], FILTER_VALIDATE_EMAIL)) ? "Por favor, informe um e-mail válido! ? 2 " : NULL;

    $verifyLoginEmail = Login::where('email', $request['email'])->select('idUsuario','email')->first() ;

    $response['emailLoginEmail'] 	= (is_null($verifyLoginEmail) )  ? "E-mail ou usuário inexistente ? 3" : NULL ;
  }

  $response = array_values( array_filter($response));
  if(!empty($response)) return AppController::retorno( $response, 400);

  $codigo = AppController::random();
  $solicitacoes =
  [
    'tipo' => 'senha',
    'codigo' =>  $codigo,
    'idUsuario' => $verifyLoginEmail['idUsuario']
  ];
  $teste = Solicitacoes::where('idUsuario', $verifyLoginEmail['idUsuario'])->where('tipo', 'senha')->where('ativo', '0')->delete();
  Solicitacoes::add( $solicitacoes );
  try
  {
    Mail::send('emails.welcome',
    array(
      'codigo' => $codigo,
      'email' => Input::get('email')
    ),
    function($message)
    {
      $message->to(
      Input::get('email') //Destinatario
      ,
      Input::get('email') //Para
      )->subject('Bem-vindo ao FolkSpace');
    });
  }
  finally
  {

  }
  $result['message'] = 'Verifique sua caixa de entrada';
  return AppController::retorno( $result );
  }
  /**
   * @api {put} /auth/password/forget 2 - Alterar
   * @apiVersion 1.0.0
   * @apiDescription Para o usuário alterar a senha.
   * @apiGroup Esqueci a senha
   * @apiName 2 - Alterar
   * @apiHeader {String} token Token fornecido pela FolkSpace.
   *
   * @apiParamExample {json} Tipo
   * {
   *     "codigo" : "STRING",
   *     "senha"  : "STRING"
   * }
   * @apiParamExample {json} Exemplo
   * {
   *     "codigo" : "696969",
   *     "senha"  : "thriller"
   * }
   * @apiError (Header) {Integer} 400 Bad Request.
   * @apiError (Parâmetros) {Integer} message Mensagem do Erro.
   * @apiError (Parâmetros) {Integer} code Codigo do erro.
   * @apiErrorExample {json} Tipo
   * {
   *     "errors": [
   *         {
   *             "message": "STRING",
   *             "code": INT
   *         }
   *     ]
   * }
   * @apiErrorExample {json} Erro
   * {
   *     "errors": [
   *         {
   *             "message": "Por favor, informe o codigo",
   *             "code": 1
   *         },
   *         {
   *             "message": "Por favor, informe a senha ",
   *             "code": 1
   *         }
   *     ]
   * }
   * @apiError (Parâmetros) {Integer} idUsuario Id do Usuário.
   * @apiSuccess (Header) {Integer} 200 Ok.
   * @apiSuccessExample {json} Tipo
   *  {
   *      "message": "STRING"
   *  }
   * @apiSuccessExample {json} Exemplo
   *  {
   *       "message": "Senha alterada!"
   *  }
   */
  function forgetPasswordChange()
  {
    $request = AppController::verificaRequest( Input::all() );
    if($request['status'] != 200)   return AppController::retorno($request['message'], 400 );
    $request = $request["result"];

    $response['codigo'] 	= (empty($request['codigo'])) 	? "Por favor, informe o codigo ? 1" : NULL ;
    $response['senha'] 	  = (empty($request['senha'])) 	? "Por favor, informe a senha ? 1" : NULL ;

    $response = array_values( array_filter($response));
    if(!empty($response)) return AppController::retorno( $response, 400);

    $forgetPasswordVerify =  Solicitacoes::where('codigo', $request['codigo']  )->where('tipo', 'senha'  )->first();
    // Erros
    if(is_null($forgetPasswordVerify)) return AppController::retorno( ['Código inválido ? 99'], 400);
    if($forgetPasswordVerify->toArray()['ativo'] != '0') return AppController::retorno( ['Código expirou ? 99'], 400);
    // Altera a senha
    Login::where('idUsuario', $forgetPasswordVerify->toArray()['idUsuario'] )->update( ['senha' => md5($request['senha']) ] );
    //SUCESSO
    Solicitacoes::where('idUsuario', $forgetPasswordVerify->toArray()['idUsuario'])->where('tipo', 'senha')->delete();
    $result['message'] = 'Senha alterada!';
    return AppController::retorno( $result );
  }

}
