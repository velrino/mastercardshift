<?php

class TokenController extends BaseController {

	public function index()
	{
			return Response::json(array('name' => 'Steve', 'state' => 'CA'));
	}

  static function Check()
	{
    $token = null;
		//  dd(Request::header());
    $header = array_change_key_case(getallheaders(),CASE_LOWER);
    if(!empty($header['token']))  $token =  $header['token'];
    if(!empty( $_GET["token"] ))  $token =  $_GET["token"];
    if(empty($token)) return TokenController::Required();
    $verifyToken = Token::getToken( $token );
    if(is_null($verifyToken)) return TokenController::Invalid();
    return true;
	}

 static function Required()
  {
    return AppController::retorno( ['Token não informado!' ] , 400);
  }

 static function Invalid()
  {
    return AppController::retorno( ['Token inválido!' ] , 400);
  }

 static function getToken($id, $idOneSignal)
	{
		$request['idUsuario'] = $id;
    //$request['device'] = Agent::platform();
		//$request['idOneSignal'] = $idOneSignal;
    $verifyToken = Token::checkToken( $request );
		if(is_null($verifyToken)) return TokenController::addToken($request);
		return TokenController::updateToken($request);
	}

	static function generateToken()
	{
	//	$request['device'] = Agent::platform();
		$request['token'] = AppController::token();
		return $request['token'];
	}

	static function addToken($request)
	{
		$request['token'] = TokenController::generateToken($request);
		$addToken = Token::addToken($request);
		return $addToken;
	}

	static function updateToken($request)
	{
		$request['token'] = TokenController::generateToken($request);
		$updateToken = Token::updateToken($request);
		return $request;
	}

}
